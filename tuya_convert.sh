#!/bin/bash

SUDO=''
if [[ $(id -u) -ne 0 ]]; then
    SUDO='sudo'
fi


echo
echo "**************************************************"
echo "* Setting hostname                               *"
echo "**************************************************"
echo "TuyaConvert" | $SUDO tee /etc/hostname
$SUDO sed -i "s/localhost/localhost TuyaConvert/" /etc/hosts


echo
echo "**************************************************"
echo "* Updating linux                                 *"
echo "**************************************************"
$SUDO apt-get update
$SUDO apt-get install -y python-pip


echo
echo "**************************************************"
echo "* Get ESPTool                                    *"
echo "**************************************************"
pip install esptool


echo
echo "**************************************************"
echo "* Get Tuya Convert Donor                         *"
echo "**************************************************"
cd ~
git clone https://github.com/digiblur/Tuya-Convert-Donor.git


echo
echo "**************************************************"
echo "* Erase flash and upload tuya convert donor      *"
echo "**************************************************"
/home/pi/.local/bin/esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --erase-all --flash_size=detect 0 /home/pi/Tuya-Convert-Donor/tc_donor_mini.generic_1M.bin


echo
echo "**************************************************"
echo "* Get Tuya Convert                               *"
echo "**************************************************"
git clone https://github.com/ct-Open-Source/tuya-convert


echo
echo "**************************************************"
echo "* Install Tuya Convert                           *"
echo "**************************************************"
cd ~/tuya-convert
./install_prereq.sh

$SUDO reboot
